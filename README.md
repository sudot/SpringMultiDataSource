# SpringMultiDataSource

### 创建MySQL数据库
```
CREATE DATABASE IF NOT EXISTS multi_a CHARACTER SET utf8 COLLATE utf8_bin;
CREATE DATABASE IF NOT EXISTS multi_b CHARACTER SET utf8 COLLATE utf8_bin;
CREATE DATABASE IF NOT EXISTS multi_c CHARACTER SET utf8 COLLATE utf8_bin;

CREATE TABLE `_User` (
`id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键' ,
`name` varchar(255) NOT NULL DEFAULT "" COMMENT '姓名' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin
COMMENT='用户表'
AUTO_INCREMENT=1
ROW_FORMAT=COMPACT
;
```
系统运行后会自动在指定的默认数据源创建表结构，默认数据源的指定方式见`spring-application-context.xml`->`dynamicDataSource`->`defaultTargetDataSource`

### 使用方式

运行单元测试类`SpringTest`
